import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    Image,
    ScrollView
} from 'react-native';
import SelectInput from 'react-native-select-input-ios';

export default class FormReciclador extends Component{
    constructor(){
        super()

        this.state = {
            cedula : '',
            nombres : '',
            apellidos : '',
            direccion : '',
            celular : '',
            correo : '',
            estadoCivil : '',
            cantidadHijos : '',
            cuentaRut : '',
            manejaOtrosCa : '',
            cantidadCa: '',
            fuenteMaterialRc: '',
            eps: ''
        }
    }
    changeCedula(cedula){
        this.setState({cedula})
    }
    changeNombres(nombres){
        this.setState({nombres})
    }
    changeApellidos(apellidos){
        this.setState({apellidos})
    }
    changeDireccion(direccion){
        this.setState({direccion})
    }
    changeCelular(celular){
        this.setState({celular})
    }
    changeCorreo(correo){
        this.setState({correo})
    }
    changeEstadoCivil(estadoCivil){
        this.setState({estadoCivil})
    }
    changeCantidadHijos(cantidadHijos){
        this.setState({cantidadHijos})
    }
    changeCuentaRut(cuentaRut){
        this.setState({cuentaRut})
    }
    changeManejaOtrosCa(manejaOtrosCa){
        this.setState({manejaOtrosCa})
    }
    changeCantidadCa(cantidadCa){
        this.setState({cantidadCa})
    }
    changeFuenteMaterialRc(fuenteMaterialRc){
        this.setState({fuenteMaterialRc})
    }
    changeEps(eps){
        this.setState({eps})
    }

    desicion() {
        return [
          {value: 0, label: 'Si'},
          {value: 1, label: 'No'},
        ];
      }

    render(){
        return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.containerImputs}>
                    <Text style={styles.textQuestion}>Cédula</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cedula}
                        onChangeText={(cedula) => this.changeCedula (cedula)}
                    />
                    <Text style={styles.textQuestion}>Nombres</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.nombres}
                        onChangeText={(nombres) => this.changeNombres (nombres)}
                    />
                    <Text style={styles.textQuestion}>Apellidos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.apellidos}
                        onChangeText={(apellidos) => this.changeApellidos (apellidos)}
                    />
                    <Text style={styles.textQuestion}>Dirección</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.direccion}
                        onChangeText={(direccion) => this.changeDireccion (direccion)}
                    />
                    <Text style={styles.textQuestion}>Celular</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.celular}
                        onChangeText={(celular) => this.changeCelular (celular)}
                    />
                    <Text style={styles.textQuestion}>Correo</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.correo}
                        onChangeText={(correo) => this.changeCorreo (correo)}
                    />
                    <Text style={styles.textQuestion}>Estado Civil</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.estadoCivil}
                        onChangeText={(estadoCivil) => this.changeEstadoCivil (estadoCivil)}
                    />
                    <Text style={styles.textQuestion}>Cantidad de Hijos</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadHijos}
                        onChangeText={(cantidadHijos) => this.changeCantidadHijos (cantidadHijos)}
                    />
                    <Text style={styles.textQuestion}>¿Cuenta con RUT?</Text>
                    <SelectInput style={styles.inputText}
                        options={this.desicion()}
                        value={this.state.cuentaRut}
                        onChangeText={(cuentaRut) => this.changeCuentaRut (cuentaRut)}
                    />
                    <Text style={styles.textQuestion}>¿Maneja otros CA?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.manejaOtrosCa}
                        onChangeText={(manejaOtrosCa) => this.changeManejaOtrosCa (manejaOtrosCa)}
                    />
                    <Text style={styles.textQuestion}>¿Cantidad de CA donde recoge?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.cantidadCa}
                        onChangeText={(cantidadCa) => this.changeCantidadCa (cantidadCa)}
                    />
                    <Text style={styles.textQuestion}>¿Su única fuente de ingresos es el material RC?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.fuenteMaterialRc}
                        onChangeText={(fuenteMaterialRc) => this.changeFuenteMaterialRc (fuenteMaterialRc)}
                    />
                    <Text style={styles.textQuestion}>¿Cuál es su EPS?</Text>
                    <TextInput style={styles.inputText}
                        value={this.state.eps}
                        onChangeText={(eps) => this.changeEps (eps)}
                    />
            </View>
            </ScrollView>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1    
    },
    containerImputs: {
        marginTop: 10,
        marginBottom: 30,
        marginHorizontal: 10,
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 5,
        
    },
    textQuestion: {
        fontWeight: 'bold' 
    }
})



