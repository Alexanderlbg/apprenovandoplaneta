import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    Image,
} from 'react-native';


export class Header extends Component{

    render(){
        return(
            <View style={styles.container}>
            <ImageBackground
                source={require('../../../assets/header.png')}
                style={styles.logo} 
            >
            <View style={styles.containerButton0}>
                <TouchableOpacity style={styles.buttonCerrarSesion}>
                    <Image source={ require('../../../assets/btncerrarsesion.png')}  
                    style={styles.logo0}/>
                </TouchableOpacity>
            </View>
            <Text style={styles.textTitle}>Reciclador</Text>
            </ImageBackground>
            </View>
        );
    }
}

    
const styles = StyleSheet.create({
    container:{
        flex: 1    
    },
    logo: {
        height: 108,
        width: 420
    },
    logo0: {
        height: 30,
        width: 30
    },
    textTitle: {
        marginTop: 75,
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: '#61987b',
        fontSize: 20,
        fontWeight: 'bold'
    },
    buttonCerrarSesion: {
        height: 30,
        width: 30,
        marginBottom: 5,
        justifyContent: 'center',
        backgroundColor: 'white',
        
    },
    containerButton0: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 5,
        marginHorizontal: 15,
        
    } 
})
    
export default Header;
