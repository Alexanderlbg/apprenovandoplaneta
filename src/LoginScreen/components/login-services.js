import { Linking, Alert } from "react-native";


export const setData = (client) => {
    const args = [
        0,
        {version: 1, product_write_date: null,error_write_date: null,documento_write_date: null}
    ]

    const params = {
        model: "movilgo.webservice",
        method: "asignarDatos",
        args: args,
        kwargs: {}
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        if (err) {
            console.log("Error", err);
            return false;
        }
        console.log(response)
        if (response.apklocation) {
            Alert.alert(
                "Actualización",
                "Existe una nueva versión del software, desea actualizarlo ahora",
                [
                    {
                        text: "Cancelar",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                    },
                    { text: "Actualizar", onPress: () => OpenURL(response.apklocation) }
                ],
                { cancelable: false }
            );
            return false;
        }
        return true;
    });
}
const OpenURL = async (url) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(url);
    } else {
        Alert.alert('La descarga no esta disponible');
    }
};



export const setTransaccion = (client) => {
    
    const args = [
        0,
        
    ]

    const params = {
        model: "",
        method: "",
        args: args,
        kwargs: {}
    }

    
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        console.log("Que trajo",response)
        console.log("err",response)
        return true;
    });
}