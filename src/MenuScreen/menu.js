import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    Image
} from 'react-native';

export default class Menu extends Component{
    render(){
        return(
        <View style={styles.container}>
        <ImageBackground
            source={require('./components/backgroundmenu.png')}
            style={styles.logo} 
        >
        <View style={styles.containerButton0}>
            <TouchableOpacity style={styles.buttonCerrarSesion}>
                <Image source={ require('../../assets/btncerrarsesion.png')}  
                style={styles.logo}/>
            </TouchableOpacity>
        </View>
        <View style={styles.containerButton}>
            <TouchableOpacity style={styles.button}>
                <ImageBackground source={ require('./components/menurecicladores.png')}  
                style={styles.logo}/>
                <Text style={styles.textButton}>Recicladores</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
                <ImageBackground source={ require('./components/menuadministradores.png')}  
                style={styles.logo}/>
                <Text style={styles.textButton}>Administradores</Text>
            </TouchableOpacity>
        </View>

        <View style={styles.containerButton2}>
            <TouchableOpacity style={styles.button}>
                <ImageBackground source={ require('./components/menuclientesagrupados.png')}  
                style={styles.logo}/>
                <Text style={styles.textButton}>Clientes Agrupados</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
                <ImageBackground source={ require('./components/menumateriales.png')}  
                style={styles.logo}/>
                <Text style={styles.textButton}>Materiales</Text>
            </TouchableOpacity>
        </View>     
        
        </ImageBackground>
        
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,    
    },
    containerButton0: {
        flexDirection: 'row',
        justifyContent:'flex-end',
        marginTop: 10,
        marginHorizontal: 10,
        
    },
    containerButton: {
        flexDirection: 'row',
        marginTop: 130,
        marginBottom: 5,
        marginHorizontal: 30,
        paddingHorizontal: 15,
        
    },
    containerButton2: {
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 10,
        marginHorizontal: 30,
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    logo: {
        flex: 1,
            
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 20,
        paddingVertical: 10,
        color: 'black'
    },
    button: {
        height: 170,
        width: 150,
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingVertical: 5,
        marginHorizontal: 5,
        borderRadius: 10,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    buttonCerrarSesion: {
        height: 30,
        width: 30,
        marginBottom: 5,
        justifyContent: 'center',
        backgroundColor: 'white',
        
    },
    textButton: {
        textAlign: 'center',
        justifyContent: 'flex-end',
        color: 'black',
        backgroundColor: 'transparent'
    }
})



