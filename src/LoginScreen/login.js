import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TextInput,
    Alert,
    TouchableHighlight,
    ImageBackground
} from 'react-native';

//Importaciones ScreenLogin
import Odoo from 'react-native-odoo';
import { setData,
    setTransaccion 
} from './components/login-services';
//**********************************

export default class Login extends React.Component {
   //**Constructor ScreenLogin**/
   constructor(props) {
    super(props);
    this.state = {
        username: 'admin',
        errorUsername: '',
        password: 'admin',
        errorPassword: '',
        loading: false
        
    }
}

/*****Ir a la Siguiente Ventana**/
navigateNext = (() => {
    this.props.navigation.navigate("Menu");
})

/***Alertas de Error***/

_storeData = async (key, session) => {
    try {
        await AsyncStorage.setItem(key, session);//guarda la sesion
    } catch (error) {
        Alert.alert("Error", 'Error al almacenar la sesión');
    }

};
verifyFields = () => {
    let error = false;
    if (this.state.username.trim() == "") {
        this.setState({ errorUsername: "Este campo es requerido" })
        error = true;
    }
    if (this.state.password.trim() == "") {
        this.setState({ errorPassword: "Este campo es requerido" })
        error = true;
    }
    return error;
}

/*** Conexión a la Base de Datos */
login = () => {
    this.setState({ loading: true });
    if (this.verifyFields()) {
        this.setState({ loading: false });
        return;
    }
    const client = new Odoo({
        host: '190.249.138.220',
        port: 8070,
        database: 'arcos1',
        username: this.state.username,
        password: this.state.password
    })

/*** Errores Conexión */

client.connect((err, response) => {
    if (err) {
        console.log("Error",err)
        if (err.message) {
            Alert.alert("Error", "Usuario y/o contraseña incorrectos");
        } else {
            Alert.alert("Error", "Sin conexion con el servidor");
        }
        this.setState({ loading: false });
        return;
    }
    if (Object.keys(response.user_context).length === 0){
        console.log("clave mala",response.user_context)
        Alert.alert("Error", "Usuario y/o contraseña incorrectos");
        this.setState({ loading: false });
        return;
    }

    console.log("siguio",response.user_context)
            if (setTransaccion(client)) {
                this._storeData('session', JSON.stringify(client));
                this.navigateNext();
                ToastAndroid.show("¡Bienvenido!", ToastAndroid.SHORT);
                const ids = { uid: response.uid, partner_id: response.partner_id }
                this._storeData('session_ids', JSON.stringify(ids));
            } else {
                this.setState({ loading: false });
                return;
            }

    },);
}

    render(){
        return(
            <View style={styles.container}>
            <ImageBackground
                source={require('./components/initbackground.png')}
                style={styles.logo} 
            >
            <View style={styles.containerImputs}>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username) => { this.setState({ username }) }}
                    placeholderTextColor="grey"
                    placeholder="Usuario"
                    style={styles.inputText}
                    errorMessage={this.state.errorUsername}
                    autoCapitalize = 'none'
                />
                <Text></Text>
                <TextInput
                    value={this.state.password}
                    onChangeText={(password) => { this.setState({ password }) }}
                    placeholderTextColor="grey"
                    placeholder="Password"
                    secureTextEntry = {true}
                    style={styles.inputText}
                    password={true}
                    errorMessage={this.state.errorUsername}
                />
            </View>
                <TouchableHighlight
                    disabled={this.state.loading}
                    style={[styles.loginButton, styles.button]}
                    onPress={this.login}
                    >
                    <Text style={styles.textButton}>Iniciar Sesión</Text>
                </TouchableHighlight>
            </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,    
    },
    containerImputs: {
        marginTop: 260,
        marginBottom: 30,
        marginHorizontal: 80,
    },
    logo: {
        flex: 1,
            
    },
    inputText: {
        borderWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 20,
        paddingVertical: 10,
        color: 'black'
    },
    button: {
        backgroundColor: '#61987b',
        paddingVertical: 20,
        marginHorizontal: 100,
        borderRadius: 5,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#ccc'   
    },
    textButton: {
        textAlign: 'center',
        color: 'white'
    }
})




